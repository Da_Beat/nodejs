const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const jwt = require('jsonwebtoken');
const FacebookTokenStrategy = require('passport-facebook-token');

const Users = require('./models/users');
const config = require('./config');

exports.local = passport.use(new LocalStrategy(Users.authenticate()));
passport.serializeUser(Users.serializeUser());
passport.deserializeUser(Users.deserializeUser());

exports.getToken = function(user) {
  return jwt.sign(user, config.secretKey, { expiresIn: 3600 });
};

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.secretKey
};

exports.jwtPassport = passport.use(new JwtStrategy(options,
  (jwt_payload, done) => {
    console.log('JWT payload: ', jwt_payload);
    Users.findOne({ _id: jwt_payload._id }, (err, user) => {
      if (err) {
        return done(err, false);
      } else if (user) {
        return done(null, user);
      } else {
        return done(null, false);
      };
    });
  }));

exports.verifyUser = passport.authenticate('jwt', { session: false });

exports.verifyAdmin = (req, res, next) => {
  if (req.user.admin) {
    next();
  } else {
    err = new Error('You are not authorized to perform this operation!');
    err.statusCode = 403;
    return next(err);
  };
};

exports.facebookPassport = passport.use(new FacebookTokenStrategy(
  {
    clientID: config.facebook.clientId,
    clientSecret: config.facebook.clientSecret
  },
  (accessToken, refreshToken, profile, done) => {
    Users.findOne({facebookId: profile.id}, (err, user) => {
      if (err) {
          return done(err, false);
      };
      if (!err && user !== null) {
          return done(null, user);
      } else {
        user = new Users({ username: profile.displayName });
        user.facebookId = profile.id;
        user.firstname = profile.name.givenName;
        user.lastname = profile.name.familyName;
        user.save((err, user) => {
          if (err) {
            return done(err, false);
          } else {
            return done(null, user);
          };
        });
      };
    });
  }
));