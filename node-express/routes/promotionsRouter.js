const express = require('express');
const bodyParser = require('body-parser');

const promotionsRouter = express.Router(); // create a mini-Express application

promotionsRouter.use(bodyParser.json());

promotionsRouter.route('/')
.all((req, res, next) => { // next is an optional parameter
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  next(); // continue looking for additional specifications, req and res will be passed on the next corresponding method
})
.get((req, res, next) => {
  res.end('Will send all promotions to you!');
})
.post((req, res, next) => {
  res.end(`Will add the promotion: ${req.body.name} with details: ${req.body.description}`);
})
.put((req, res, next) => {
  res.statusCode = 403;
  res.end('PUT Operation not supported on /promotions');
})
.delete((req, res, next) => {
  res.end('Deleting all the promotions!');
});

promotionsRouter.route('/:promoId')
.get((req, res, next) => {
  res.end(`Will send details of the promotion: ${req.params.promoId} to you!`);
})
.post((req, res, next) => {
  res.statusCode = 403;
  res.end(`POST Operation not supported on /promotions/${req.params.promoId}`);
})
.put((req, res, next) => {
  res.write(`Updating the promotion: ${req.params.promoId}\n`);
  res.end(`Will update the promotion: ${req.params.promoId} with details: ${req.body.description}`)
})
.delete((req, res, next) => {
  res.end(`Deleting promotion: ${req.params.promoId}`);
});

module.exports = promotionsRouter;